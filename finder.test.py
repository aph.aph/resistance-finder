import unittest
import filecmp
import os

class Test(unittest.TestCase):
    def test_finder(self):
        os.system('python3 finder.py')
        self.assertTrue(filecmp.cmp('data/pathogens.output.all.csv', 'data/pathogens.output.all.expected.csv', shallow=False), 'pathogens.output.all.csv has errors.')
        self.assertTrue(filecmp.cmp('data/pathogens.output.species.csv', 'data/pathogens.output.species.expected.csv', shallow=False), 'pathogens.output.species.csv has errors.')

if __name__ == '__main__':
    unittest.main()