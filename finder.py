import csv
import re

PATHOGEN_FILE = 'data/pathogens.csv'
RESISTANCE_GENES_FILE = 'data/resistance-genes.csv'

TOTAL_ISOLATE_COUNT_FILE_SUFFIX = 'output.all'
ORGANISM_GROUP_ISOLATE_COUNT_FILE_SUFFIX = 'output.species'

ANTIBIOTIC_INDICES = [0, 1, 2, 3, 4, 7]
CARB_INDEX = 7
RMTASE_INDEX = 5
SOC_AMINOGLYCOSID_INDICES = [0, 1, 2, 3]
FIRST_PATHOGEN_GENE_COLUMN_INDEX = 14

# I/O
# ====================================================================

def readCsvFile(filePath):
    rows = []
    with open(filePath) as csvFile:
        csvReader = csv.reader(csvFile, delimiter=';')
        for row in csvReader:
            rows.append(row)
    return rows

def writeCsvFile(filePath, rows):
    with open(filePath, 'w', newline='') as csvFile:
        csvWriter = csv.writer(csvFile, delimiter=';')
        for row in rows:
            csvWriter.writerow(row)

# Build up data structures
# ====================================================================

def extractAllGenes(resistanceGenes):
    allGenes = set()
    for row in resistanceGenes:
        for cell in row:
            if cell != '':
                geneWithPattern = (cell, re.compile(re.escape(cell) + "(\-[A-Z]*|\-[0-9]*)?[a-z]?(-.*)?(=.*)?$"))
                allGenes.add(geneWithPattern)
    return allGenes

def extractSubstanceGenes(resistanceGenes, substanceIndex):
    genes = set([row[substanceIndex] for _, row in enumerate(resistanceGenes) if row[substanceIndex] != ''])
    return genes

def extractAllPathogenRows(pathogens):
    rows = set([row for row, pathogen in enumerate(pathogens)])
    return rows

def buildGene2PathogenRowLookup(genesWithPatterns, pathogens):
    gene2PathogenRowLookup = {}
    
    pathogensCount = len(pathogens)
    for row, pathogen in enumerate(pathogens):
        if row % 10000 == 0:
            print("%2.0f%%" % ((100 * row) / pathogensCount), end = "\r")
        for gene, pattern in genesWithPatterns:
            if containsAnyResistanceGene(pathogen, pattern, gene):
                gene2PathogenRowLookup.setdefault(gene, set()).add(row)
    
    print("", end = "\r")
    return gene2PathogenRowLookup

def buildSpecies2PathogenRowLookup(pathogens):
    species2PathogenRowLookup = {}

    pathogensCount = len(pathogens)
    for row, pathogen in enumerate(pathogens):
        if row % 10000 == 0:
            print("%2.0f%%" % ((100 * row) / pathogensCount), end = "\r")
        species2PathogenRowLookup.setdefault(pathogen[0], set()).add(row)
    
    print("", end = "\r")
    return species2PathogenRowLookup

def containsAnyResistanceGene(pathogen, pattern, gene):
    for pathogenGene in pathogen[FIRST_PATHOGEN_GENE_COLUMN_INDEX:]:
        if pattern.search(pathogenGene) != None:
            return True
    return False

# Evaluate
# ====================================================================

def totalPathogensCount(pathogens):
    return len(pathogens)

def speciesCount(species2PathogenRowLookup):
    return len(species2PathogenRowLookup)

def resistantPathogenRows(gene2PathogenRowLookup, resistanceGenes):
    resistantPathogenRows = set()
    for gene in resistanceGenes:
        resistantPathogenRows = resistantPathogenRows.union(gene2PathogenRowLookup.get(gene, set()))
    return resistantPathogenRows

def susceptiblePathogenRows(gene2PathogenRowLookup, resistanceGenes, allPathogenRows):
    resistantRows = resistantPathogenRows(gene2PathogenRowLookup, resistanceGenes)
    susceptiblePathogenRows = allPathogenRows.difference(resistantRows)
    return susceptiblePathogenRows

# Run
# ====================================================================

print("Start building gene index...")
pathogens = readCsvFile(PATHOGEN_FILE)[1:]
resistanceGeneRows = readCsvFile(RESISTANCE_GENES_FILE)

substances = resistanceGeneRows[0]
resistanceGenes = resistanceGeneRows[1:]

print("Extract resistance genes...")
allGenes = extractAllGenes(resistanceGenes)

print("Extract pathogen rows...")
allPathogenRows = extractAllPathogenRows(pathogens)

print("Build pathogen row lookup by resistance genes...")
gene2PathogenRowLookup = buildGene2PathogenRowLookup(allGenes, pathogens)

print("Build pathogen row lookup by species...")
species2PathogenRowLookup = buildSpecies2PathogenRowLookup(pathogens)

print("Gene index complete.")


# Total isolate count file
totalIsolateCountFileRows = []

pathogensCount = len(pathogens)
print("# isolates: ", pathogensCount)
totalIsolateCountFileRows.append([ "# isolates", pathogensCount ])

speciesCount = len(species2PathogenRowLookup)
print("# organism groups: ", speciesCount)
totalIsolateCountFileRows.append([ "# organism groups", speciesCount ])

for substanceIndex in ANTIBIOTIC_INDICES:
    genes = extractSubstanceGenes(resistanceGenes, substanceIndex)
    genesPathogenRows = resistantPathogenRows(gene2PathogenRowLookup, genes)

    print("{}: # resistant isolates: {}".format(substances[substanceIndex], len(genesPathogenRows)))
    totalIsolateCountFileRows.append([ substances[substanceIndex], len(genesPathogenRows) ])

for substanceIndex in ANTIBIOTIC_INDICES:
    genes = extractSubstanceGenes(resistanceGenes, substanceIndex)
    genesPathogenRows = susceptiblePathogenRows(gene2PathogenRowLookup, genes, allPathogenRows)

    print("Not {}: # resistant isolates: {}".format(substances[substanceIndex], len(genesPathogenRows)))

    totalIsolateCountFileRows.append([ "Not {}".format(substances[substanceIndex]), len(genesPathogenRows) ])

carbGenes = extractSubstanceGenes(resistanceGenes, CARB_INDEX)
carbPathogenRows = resistantPathogenRows(gene2PathogenRowLookup, carbGenes)

for substanceIndex in ANTIBIOTIC_INDICES:
    substanceGenes = extractSubstanceGenes(resistanceGenes, substanceIndex)
    substancePathogenRows = resistantPathogenRows(gene2PathogenRowLookup, substanceGenes)

    genesPathogenRows = substancePathogenRows.intersection(carbPathogenRows)
    
    print("{} + carb: # resistant isolates: {}".format(substances[substanceIndex], len(genesPathogenRows)))
    totalIsolateCountFileRows.append([ "{} + carb".format(substances[substanceIndex]), len(genesPathogenRows) ])

for substanceIndex in ANTIBIOTIC_INDICES:
    substanceGenes = extractSubstanceGenes(resistanceGenes, substanceIndex)
    substancePathogenRows = susceptiblePathogenRows(gene2PathogenRowLookup, substanceGenes, allPathogenRows)

    genesPathogenRows = substancePathogenRows.intersection(carbPathogenRows)
    
    print("Not {} + carb: # resistant isolates: {}".format(substances[substanceIndex], len(genesPathogenRows)))
    totalIsolateCountFileRows.append([ "Not {} + carb".format(substances[substanceIndex]), len(genesPathogenRows) ])

socAminoglycosidPathogenRowSets = []
for substanceIndex in SOC_AMINOGLYCOSID_INDICES:
    substanceGenes = extractSubstanceGenes(resistanceGenes, substanceIndex)
    substancePathogenRows = resistantPathogenRows(gene2PathogenRowLookup, substanceGenes)
    socAminoglycosidPathogenRowSets.append(substancePathogenRows)

socAminoglycosidPathogenRowUnion = set()
for genesPathogenRows in socAminoglycosidPathogenRowSets:
    socAminoglycosidPathogenRowUnion = socAminoglycosidPathogenRowUnion.union(genesPathogenRows)

print("Any SOC Aminoclycosid: # resistant isolates: {}".format(len(socAminoglycosidPathogenRowUnion)))
totalIsolateCountFileRows.append([ "Any SOC Aminoclycosid", len(socAminoglycosidPathogenRowUnion) ])

socAminoglycosidPathogenRowIntersection = socAminoglycosidPathogenRowUnion
for genesPathogenRows in socAminoglycosidPathogenRowSets:
    socAminoglycosidPathogenRowIntersection = socAminoglycosidPathogenRowIntersection.intersection(genesPathogenRows)

print("All SOC Aminoclycosids: # resistant isolates: {}".format(len(socAminoglycosidPathogenRowIntersection)))
totalIsolateCountFileRows.append([ "All SOC Aminoclycosids", len(socAminoglycosidPathogenRowIntersection) ])

rmtaseGenes = extractSubstanceGenes(resistanceGenes, RMTASE_INDEX)
rmtasePathogenRows = resistantPathogenRows(gene2PathogenRowLookup, rmtaseGenes)

print("RMTase: # resistant isolates: {}".format(len(rmtasePathogenRows)))
totalIsolateCountFileRows.append([ "RMTase", len(rmtasePathogenRows) ])

writeCsvFile(PATHOGEN_FILE[:-4] + "." + TOTAL_ISOLATE_COUNT_FILE_SUFFIX + ".csv", totalIsolateCountFileRows)

# Organism group isolate count file
organismGroupIsolateCountRows = []

for species, speciesPathogenRows in species2PathogenRowLookup.items():
    print("{}: {}".format(species, len(list(speciesPathogenRows))))

organismGroupIsolateCountRows.append([ "" ] + list(species2PathogenRowLookup.keys()))
organismGroupIsolateCountRows.append([ "Count" ] + list(map(lambda species: len(species), list(species2PathogenRowLookup.values()))))

for species, speciesPathogenRows in species2PathogenRowLookup.items():
    for substanceIndex in ANTIBIOTIC_INDICES:
        genes = extractSubstanceGenes(resistanceGenes, substanceIndex)
        
        resistantGenePathogenRows = resistantPathogenRows(gene2PathogenRowLookup, genes)
        print("{}, {}: # resistant isolates: {}".format(species, substances[substanceIndex], len(speciesPathogenRows.intersection(resistantGenePathogenRows))))

        susceptibleGenePathogenRows = susceptiblePathogenRows(gene2PathogenRowLookup, genes, allPathogenRows)
        print("Not {}, {}: # resistant isolates: {}".format(species, substances[substanceIndex], len(speciesPathogenRows.intersection(susceptibleGenePathogenRows))))

    print("{}, Any SOC Aminoclycosid: # resistant isolates: {}".format(species, len(speciesPathogenRows.intersection(socAminoglycosidPathogenRowUnion))))
    print("{}, All SOC Aminoclycosids: # resistant isolates: {}".format(species, len(speciesPathogenRows.intersection(socAminoglycosidPathogenRowIntersection))))

for substanceIndex in ANTIBIOTIC_INDICES:
    genes = extractSubstanceGenes(resistanceGenes, substanceIndex)
    
    resistantGenePathogenRows = resistantPathogenRows(gene2PathogenRowLookup, genes)
    resistantCountsPerSpecies = map(lambda speciesPathogenRows: len(resistantGenePathogenRows.intersection(speciesPathogenRows)), species2PathogenRowLookup.values())
    organismGroupIsolateCountRows.append([ substances[substanceIndex] ] + list(resistantCountsPerSpecies))

for substanceIndex in ANTIBIOTIC_INDICES:
    genes = extractSubstanceGenes(resistanceGenes, substanceIndex)

    susceptibleGenePathogenRows = susceptiblePathogenRows(gene2PathogenRowLookup, genes, allPathogenRows)
    susceptibleCountsPerSpecies = map(lambda speciesPathogenRows: len(susceptibleGenePathogenRows.intersection(speciesPathogenRows)), species2PathogenRowLookup.values())
    organismGroupIsolateCountRows.append([ "Not {}".format(substances[substanceIndex]) ] + list(susceptibleCountsPerSpecies))

socAminoglycosidPathogenRowsUnionPerSpecies = map(lambda speciesPathogenRows: len(socAminoglycosidPathogenRowUnion.intersection(speciesPathogenRows)), species2PathogenRowLookup.values())
organismGroupIsolateCountRows.append([ "Any SOC Aminoclycosid" ] + list(socAminoglycosidPathogenRowsUnionPerSpecies))

socAminoglycosidPathogenRowsIntersectionPerSpecies = map(lambda speciesPathogenRows: len(socAminoglycosidPathogenRowIntersection.intersection(speciesPathogenRows)), species2PathogenRowLookup.values())
organismGroupIsolateCountRows.append([ "All SOC Aminoclycosids" ] + list(socAminoglycosidPathogenRowsIntersectionPerSpecies))

for species, speciesPathogenRows in species2PathogenRowLookup.items():
    for substanceIndex in ANTIBIOTIC_INDICES:
        genes = extractSubstanceGenes(resistanceGenes, substanceIndex)
        
        resistantGenePathogenRows = resistantPathogenRows(gene2PathogenRowLookup, genes)
        print("{}, {} + carb: # resistant isolates: {}".format(species, substances[substanceIndex], len(speciesPathogenRows.intersection(resistantGenePathogenRows).intersection(carbPathogenRows))))

        susceptibleGenePathogenRows = susceptiblePathogenRows(gene2PathogenRowLookup, genes, allPathogenRows)
        print("Not {}, {} + carb: # resistant isolates: {}".format(species, substances[substanceIndex], len(speciesPathogenRows.intersection(susceptibleGenePathogenRows).intersection(carbPathogenRows))))

    print("{}, Any SOC Aminoclycosid + carb: # resistant isolates: {}".format(species, len(speciesPathogenRows.intersection(socAminoglycosidPathogenRowUnion).intersection(carbPathogenRows))))
    print("{}, All SOC Aminoclycosids + carb: # resistant isolates: {}".format(species, len(speciesPathogenRows.intersection(socAminoglycosidPathogenRowIntersection).intersection(carbPathogenRows))))

for substanceIndex in ANTIBIOTIC_INDICES:
    genes = extractSubstanceGenes(resistanceGenes, substanceIndex)
    
    resistantGenePathogenRows = resistantPathogenRows(gene2PathogenRowLookup, genes)
    resistantCountsPerSpecies = map(lambda speciesPathogenRows: len(resistantGenePathogenRows.intersection(speciesPathogenRows).intersection(carbPathogenRows)), species2PathogenRowLookup.values())
    organismGroupIsolateCountRows.append([ "{} + carb".format(substances[substanceIndex]) ] + list(resistantCountsPerSpecies))

for substanceIndex in ANTIBIOTIC_INDICES:
    genes = extractSubstanceGenes(resistanceGenes, substanceIndex)

    susceptibleGenePathogenRows = susceptiblePathogenRows(gene2PathogenRowLookup, genes, allPathogenRows)
    susceptibleCountsPerSpecies = map(lambda speciesPathogenRows: len(susceptibleGenePathogenRows.intersection(speciesPathogenRows).intersection(carbPathogenRows)), species2PathogenRowLookup.values())
    organismGroupIsolateCountRows.append([ "Not {} + carb".format(substances[substanceIndex]) ] + list(susceptibleCountsPerSpecies))

socAminoglycosidPathogenRowsUnionPerSpecies = map(lambda speciesPathogenRows: len(socAminoglycosidPathogenRowUnion.intersection(speciesPathogenRows).intersection(carbPathogenRows)), species2PathogenRowLookup.values())
organismGroupIsolateCountRows.append([ "Any SOC Aminoclycosid + carb" ] + list(socAminoglycosidPathogenRowsUnionPerSpecies))

socAminoglycosidPathogenRowsIntersectionPerSpecies = map(lambda speciesPathogenRows: len(socAminoglycosidPathogenRowIntersection.intersection(speciesPathogenRows).intersection(carbPathogenRows)), species2PathogenRowLookup.values())
organismGroupIsolateCountRows.append([ "All SOC Aminoclycosids + carb" ] + list(socAminoglycosidPathogenRowsIntersectionPerSpecies))

for species, speciesPathogenRows in species2PathogenRowLookup.items():
    print("{}, RMTase: # resistant isolates: {}".format(species, len(rmtasePathogenRows.intersection(speciesPathogenRows))))

rmtasePathogenRowsPerSpecies = map(lambda speciesPathogenRows: len(rmtasePathogenRows.intersection(speciesPathogenRows)), species2PathogenRowLookup.values())
organismGroupIsolateCountRows.append([ "RMTase" ] + list(rmtasePathogenRowsPerSpecies))

writeCsvFile(PATHOGEN_FILE[:-4] + "." + ORGANISM_GROUP_ISOLATE_COUNT_FILE_SUFFIX + ".csv", organismGroupIsolateCountRows)